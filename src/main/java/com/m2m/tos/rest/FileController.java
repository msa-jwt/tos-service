package com.m2m.tos.rest;

import java.io.IOException;
import java.util.List;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;




import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value="File Service API")
@RequestMapping(value="/api/v1/file")
@RestController
public class FileController {
	
	@ApiOperation(value="check_alive ")
	@RequestMapping(value="/check_alive", method=RequestMethod.GET)
	public ResponseEntity <String > getTest(){
		return new ResponseEntity<String> (" I'am alive ", HttpStatus.OK);
	}
	/**
	 * file upload
	 * @param userId
	 * @param userNm
	 * @param addr
	 * @param file
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value="file 저장 하기 ")
	@RequestMapping(value="/upload", method=RequestMethod.POST)
	public ResponseEntity <String > setUserInsert(
			@RequestParam("userId") String userId,
			@RequestParam("userNm") String userNm,
			@RequestParam("addr") String addr,
			@RequestParam("mfile") MultipartFile mfile
		) throws Exception { 
		
		
		File file = new File();
		List<File> list = null;
		
		file.setFile(mfile.getBytes());
		file.setFileName(mfile.getOriginalFilename());
		
		
		log.info("Start db insert");
//		int re  = sampleFileDao.insertFile(file);
//		log.debug("result :"+ re);
		
		return new ResponseEntity<String> ("", HttpStatus.OK);
	}
	
	
	/**
	 * file download
	 * @param userId
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(path = "/download/{fileId}", method = RequestMethod.GET)
	public ResponseEntity<Resource> download(
			@PathVariable (name="fileId", required = true) String fileId
		) throws IOException {
		
		
		//SampleUser re = null;
		File re = new File();
		try {
			log.info("Start db select");
		//	re = sampleFileDao.selectFileById(userId);

		} catch (Exception e) {
			e.printStackTrace();
		}

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		headers.add("Content-disposition", "attachment; filename=\"" + re.getFileName() + "\"");
//		Path path = Paths.get(file.getAbsolutePath());
		ByteArrayResource resource = new ByteArrayResource(re.getFile());

		
		return ResponseEntity
				.ok()
				.headers(headers)
//				.contentLength(resource.getFile().length())
				.contentType(MediaType.parseMediaType("application/octet-stream"))
				.body(resource);
	}
}
