package com.m2m.tos.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.google.common.collect.Lists;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * jwt token 사용시 적용할 swagger 설정  
 * @author juheon
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig  {
	@Bean
	    public Docket api() {
		
		Docket docket = new Docket(DocumentationType.SWAGGER_2) 
					.apiInfo(apiInfo()) 
//					.pathMapping("/") 	
					.forCodeGeneration(true) 
					.genericModelSubstitutes(ResponseEntity.class) 
					.ignoredParameterTypes(java.sql.Date.class) 
					.directModelSubstitute(java.time.LocalDate.class, java.sql.Date.class) 
					.directModelSubstitute(java.time.ZonedDateTime.class, Date.class) 
					.directModelSubstitute(java.time.LocalDateTime.class, Date.class) 
					.securityContexts(Lists.newArrayList(securityContext())) 
					.securitySchemes(Lists.newArrayList(apiKey())) 
					.useDefaultResponseMessages(false); 
		
				docket = docket
							.select()
							.apis(RequestHandlerSelectors.basePackage("com.m2m"))
		//	                .apis(RequestHandlerSelectors.any()) 
//				            .paths(PathSelectors.regex("/api/**"))   
							.paths(PathSelectors.ant("/api/**")) 
							.build();

		return docket;

	    }
	 
	    private ApiInfo apiInfo() {
	        return new ApiInfoBuilder()
	                .title("Tos Rest API")
	                .description("Tos 서비스의 Rest API 입니다. ")
	                .build();
	 
	    }
	    

	    private ApiKey apiKey() { 
	    	return new ApiKey("JWT",  "Authorization", "header"); 
	    } 
	    
	    private List<SecurityContext> securityContext() { 
	    	return Lists.newArrayList(springfox.documentation.spi.service.contexts.SecurityContext
	    			.builder() 
	    			.securityReferences(defaultAuth()) 
	    			.forPaths(PathSelectors.any()) 
	    			.build()); 
	    	
	    } 
	    
	    List<SecurityReference> defaultAuth() { 
	    	AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything"); 
	    	AuthorizationScope[] authorizationScopes = new AuthorizationScope[1]; 
	    	authorizationScopes[0] = authorizationScope; 
	    	return Lists.newArrayList(new SecurityReference("JWT", authorizationScopes));
	    	
	    }
	    
	    
}
