package com.m2m.tos.rest;

import java.io.IOException;
import java.util.List;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;




import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value="Live")
@RequestMapping(value="/api/healthy")
@RestController
public class HealthyController {
	
	@ApiOperation(value="check_alive ")
	@RequestMapping(value="/check", method=RequestMethod.GET)
	public ResponseEntity <String > getCheckAlive(){
		return new ResponseEntity<String> (" I'am alive ", HttpStatus.OK);
	}
}
