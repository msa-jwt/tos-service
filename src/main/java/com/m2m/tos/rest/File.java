package com.m2m.tos.rest;

import lombok.Data;

@Data
public class File {
	 // file info
	 private String fileName;
	 private byte[] file;

}
