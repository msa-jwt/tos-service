package com.m2m.tos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages="com.m2m")
@SpringBootApplication
public class TosServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TosServiceApplication.class, args);
	}

}
