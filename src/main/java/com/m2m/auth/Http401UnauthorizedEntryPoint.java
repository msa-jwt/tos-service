package com.m2m.auth;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RestControllerAdvice
//public class Http401UnauthorizedEntryPoint implements AuthenticationEntryPoint {
public class Http401UnauthorizedEntryPoint extends ResponseEntityExceptionHandler {


//    /**
//     * Always returns a 401 error code to the client.
//     */
//    @Override
//    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException arg2) throws IOException,
//            ServletException {
//
//        log.debug("Pre-authenticated entry point called. Rejecting access");
//        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Access Denied");
//    }
    
//    @ExceptionHandler (value = {AccessDeniedException.class})
//    public void commence(HttpServletRequest request, HttpServletResponse response,
//        AccessDeniedException accessDeniedException) throws IOException {
//    	log.debug("401 error");
//      // 401
//      response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authorization Failed : " + accessDeniedException.getMessage());
//    }
//    
//    @ExceptionHandler (value = {ExpiredJwtException.class})
//    public void commence(HttpServletRequest request, HttpServletResponse response,
//    		ExpiredJwtException accessDeniedException) throws IOException {
//    	log.debug("401 error");
//      // 401
//      response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authorization Failed : " + accessDeniedException.getMessage());
//    }
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse error = new ErrorResponse("Server Error", details);
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
//    @ExceptionHandler(ExpiredJwtException.class)
//    public ResponseEntity<Object> expiredJwtException(
//    		ExpiredJwtException ex, WebRequest request) {
//
//    	log.debug("handleCityNotFoundException");
//        Map<String, Object> body = new LinkedHashMap<>();
//        body.put("timestamp", LocalDateTime.now());
//        body.put("message", "City not found");
//
//        return new ResponseEntity<>(body, HttpStatus.UNAUTHORIZED);
//    }
    
    @ExceptionHandler(ServletException.class)
    public ResponseEntity<Object> servletException(
    		ServletException ex, WebRequest request) {

    	log.debug("servletException");
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", "City not found");

        return new ResponseEntity<>(body, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = { ExpiredJwtException.class, IllegalArgumentException.class, IllegalStateException.class })
    protected ResponseEntity<Object> handleConflict(
    		Exception ex, HttpServletRequest  request) {
    	log.debug("handleConflict");
    	log.debug("servletException");
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", "City not found");
    	return new ResponseEntity<>(body, HttpStatus.UNAUTHORIZED);
    }
    
    
}