package com.m2m.auth;


import lombok.Data;
import lombok.EqualsAndHashCode;



@Data
public class AuthUser {

    private long id;
    private String username;
    private String password;
    private String email;
    private final String jwt;
    private boolean active = true;  // 나중에 바꿀것 
    private String roles;
    
    
    
    public AuthUser() {
		this.jwt = "";
    	
    }
    
    public AuthUser(String jwt) {
		this.jwt = jwt;
    }



}