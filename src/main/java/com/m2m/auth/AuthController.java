package com.m2m.auth;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUtil jwtTokenUtil;


	@RequestMapping(value= "/hello", method = RequestMethod.GET )
	public String firstPage() {
		return "Hello World";
	}

	
	
	/**
	 * 토큰에 있는 클레임 정보를 추출하기 
	 * @param request
	 * @return
	 */
	@RequestMapping(value= "/extract/claims", method = RequestMethod.GET )
	public ResponseEntity<?>  getExtractEmail(HttpServletRequest request) {
		
		return ResponseEntity.ok(jwtTokenUtil.getClaims(request));
	}
	
}
